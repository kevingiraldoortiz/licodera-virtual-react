import React from "react";
import BarraNavegacion from './BarraNavegacion';
import Encabezado from './Encabezado';

const MiCarrito = () =>{
    return(
     <div>
        <Encabezado/>
        <BarraNavegacion/>
        <h1>MiCarrito</h1>
     </div>
    );
}

export default MiCarrito;