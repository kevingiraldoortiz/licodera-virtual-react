import React from "react";

const Whisky = () =>{
    return (
        <div class="targetas">
            <div class="targeta">
                <div class="whisky1">
                    <div class="imagen_whisky1"></div>
                    <div class="detalle">
                        <h2>Blue Label 750ml</h2>
                        <p>Whisky de 750ml en envase de vidrio.</p>
                    </div>
                </div>
            </div>

            <div class="targeta">
                <div class="whisky2">
                    <div class="imagen_whisky2"></div>
                    <div class="detalle">
                        <h2>Jack Daniels 3L</h2>
                        <p>Whisky de 3L en envase de vidrio.</p>
                    </div>
                </div>
            </div>

            <div class="targeta">
                <div class="whisky3">
                    <div class="imagen_whisky3"></div>
                    <div class="detalle">
                        <h2>Buchanan's 750ml</h2>
                        <p>Whisky de 750ml en envase de vidrio.</p>
                    </div>
                </div>
            </div>
        </div>
    );
}


export default Whisky;