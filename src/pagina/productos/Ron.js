import React from "react";

const Ron = () =>{
    return (
        <div class="targetas">
            <div class="targeta">
                <div class="ron1">
                    <div class="imagen_ron1"></div>
                    <div class="detalle">
                        <h2>Ron Medellin 750ml</h2>
                        <p>Ron de 750ml en envase de vidrio.</p>
                    </div>
                </div>
            </div>

            <div class="targeta">
                <div class="ron2">
                    <div class="imagen_ron2"></div>
                    <div class="detalle">
                        <h2>Viejo de Caldas 750ml</h2>
                        <p>Ron de 750ml en envase de vidrio.</p>
                    </div>
                </div>
            </div>

            <div class="targeta">
                <div class="ron3">
                    <div class="imagen_ron3"></div>
                    <div class="detalle">
                        <h2>La Hechicera 700ml</h2>
                        <p>Ron de 700ml en envase de vidrio.</p>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Ron;