import React from "react";
import BarraNavegacion from './BarraNavegacion';
import Encabezado from './Encabezado';

const MiCuenta = () =>{
    return (
        <div>
        <Encabezado/>
        <BarraNavegacion/>
        <h1>MiCuenta</h1>
        </div>
    );
}

export default MiCuenta;